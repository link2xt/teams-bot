#!/usr/bin/env bash

python3 -m venv ~/.local/lib/teams-bot.venv
source ~/.local/lib/teams-bot.venv/bin/activate
pip install -U pip wheel

